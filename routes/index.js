var express = require('express');
var router = express.Router();

var fs=require('fs');
var ini = require('ini');

/* GET home page. */
router.get('/', function(req, res, next) {
	var config = ini.parse(fs.readFileSync('setting.ini', 'utf-8'));
  res.render('index', { title: 'Jadwal Sholat', data: config});
});

module.exports = router;
